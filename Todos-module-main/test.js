// (function() {

// input, tasks, taskClassName, btnDelClassName, btnAll, btnActive, btnCompleted, block_info, btn_delete

// Функция конструктор
	// Функция принимает в качестве аргументов HTML объекты
	// input - поле ввода дела
	// tasks - блок, куда добавляются дела
	// taskClassName - Название класса для всех дел,
	// для их стилизации (необязательный)
	// btnDelClassName - название класса 
	// для кнопки удаления дела (необязательный)
	// btnAll - кнопка All
	// btnActive - кнопка Active
	// btnCompleted - кнопка Completed
	// block_info - блок с информацией о количестве дел
	// btn_delete - кнопка удаления сразу нескольких дел

function Todolists(mainTodolistId, config) {

	// Параметры функции:
	// config - Объект с конфигурациями
	// Свойства config - 
	// 		input - поле ввода дел (Обязательный)
	//		tasks - блок хранящие дела (обязательный)
	//		taskClassName - название класса, которое будет иметь каждое дело, для стилизации (не обязательный)
	//		btnDelClassName - название класса, для кнопки удаления одного дела (не обязательный)
	//		btnAll - кнопка All, для возможности удаления всех дел
	//		btnActive - кнопка Active, для возможности удаления не выполненных дел
	//		btnCompleted - кнопка completed, для возможности удаления выполненных дел
	//		blockInfo - место для отбражения количества оставшихся дел
	//		btnDelete - кнопка для удаления нескольких дел сразу

	this._mainTodosId		= mainTodolistId;
	this.$input 			= config.input;
	this.$tasks 			= config.tasks;
	this._taskClassName 	= config.taskClassName;
	this._btnDelClassName	= config.btnDelClassName;
	this._counterForId		= 0; // Будет считать количество дел которое вообще было на тудулисте
	this._counterTasks		= 1;
	this._id 				= ++Todolists._countLists;
	this.$btnAll			= config.btnAll;
	this.$btnActive			= config.btnActive;
	this.$btnCompleted		= config.btnCompleted;
	this.$blockInfo			= config.blockInfo;
	this.$btnDelete 		= config.btnDelete;

	if (this._taskClassName == undefined)
		this._taskClassName = _mainTodosId + "task";

	this._btnAllOn			= false;
	this._btnActiveOn		= false;
	this._btnCompletedOn	= false;

	this._tasks = [];

	// btn All click

	// btn Active click

	// btn Completed click

	this.$input.addEventListener("keyup", this.addTask.bind(this));
	this.$btnAll.addEventListener("click", this.btnAllClickHandler.bind(this));
	this.$btnActive.addEventListener("click", this.btnActiveClickHandler.bind(this));
	this.$btnCompleted.addEventListener("click", this.btnCompletedClickHandler.bind(this));
	this.$btnDelete.addEventListener("click", this.btnDeleteClickHandler.bind(this));

}

Todolists.prototype.addTask = function(event) {
	
	// Метод каждого объекта
	// Добавляет одно дело в блок с делами
	// Дела состоит из:
	// 		checkbox с id - (уникальный идентификатор из аргументов функции) + порядковый номер дела
	//		label -для связки текста с checkbox
	//		span для значка крестика (удаление дела)

	let taskText = this.$input.value;
	if (event.keyCode != 13 || taskText == "") return;

	this._counterForId += 1;

	// Создание дела

	let task 		= document.createElement("div"),
		checkbox 	= document.createElement("input"),
		label 		= document.createElement("label"),
		span 		= document.createElement("span");

	checkbox.type 	= "checkbox";
	checkbox.id 	= "checkbox" + this._id + this._counterForId;
	label.innerText = taskText;
	label.htmlFor	= "checkbox" + this._id + this._counterForId;
	span.innerHTML 	= "&#10006";
	span.id = this._mainTodosId + "span" + this._id + this._counterForId;

	task.append(checkbox);
	task.append(label);
	task.append(span);

	if (this._taskClassName != undefined) {
		task.className = this._taskClassName;
	}

	if (this._btnDelClassName != undefined) {
		span.className = this._btnDelClassName;
	}

	// Добавление информации о деле в массив всех дел

	let taskInformation = {

		taskBlock: 		task,
		taskCheckbox: 	checkbox,
		taskSpanId: 	span.id,

	}

	this._tasks.push(taskInformation);

	// Удаления дела по клику

	span.addEventListener("click", deleteTask.bind(this));

	// Изменение информации о количестве дел

	label.addEventListener("click", setInfoAboutTasksCount.bind(this));

	function deleteTask() {
		
		for (let task in this._tasks)
			if (this._tasks[task].taskSpanId == span.id)	{
				this._tasks.splice(task, 1);
				break;
			}

		span.parentNode.remove();
		this._counterTasks -= 1;

		// Изменение информации о количестве дел

		setInfoAboutTasksCount.call(this);

	}

	let self = this;

	function setInfoAboutTasksCount() {

		switch(true) {

			case this._btnAllOn:
				this._btnAllOn = false;
				setTimeout(this.btnAllClickHandler.bind(this), 10);
				break;

			case this._btnActiveOn: 
				this._btnActiveOn = false;
				setTimeout(this.btnActiveClickHandler.bind(this), 10);
				break;

			case this._btnCompletedOn:
				this._btnCompletedOn = false; 
				setTimeout(this.btnCompletedClickHandler.bind(this), 10);
				break;

		}

	}

	// Добавление дела в начало блока

	this.$tasks.prepend(task);
	this._counterTasks += 1;

	// Очистка поля ввода

	this.$input.value = "";

	// Изменение информации о количестве дел

	setInfoAboutTasksCount.call(this);

	console.log(this._tasks)

};

Todolists.prototype.btnAllClickHandler = function() {

	// Обработка клика по кнопке All
	// Показывает количество всех дел

	if (this._btnAllOn == true) {

		this.$btnDelete.innerText = "";
		this.$blockInfo.innerText = "";
		this.$btnAll.style.color = "#BEB6B6";

		for (let task in this._tasks) {
			this._tasks[task].taskBlock.style.boxShadow = 'none';
		}

		this._btnAllOn = false;

		return;

	}

	for (let task in this._tasks) {
		this._tasks[task].taskBlock.style.boxShadow = '0 0 5px inset';
	}

	this.$btnDelete.innerText = "Clear all tasks";

	this.$btnAll.style.color = "#D3D3D3";
	this.$btnActive.style.color = '#BEB6B6';
	this.$btnCompleted.style.color = '#BEB6B6';

	this._btnAllOn			= true;
	this._btnActiveOn		= false;
	this._btnCompletedOn	= false;

	// body..

	if (this._counterTasks - 1 == 0) {
		this.$blockInfo.innerText = "No tasks";
	} else if (this._counterTasks - 1 == 1) {
		this.$blockInfo.innerText = "1 item left";
	} else {
		this.$blockInfo.innerText = this._counterTasks - 1 + " items left";
	}

}

Todolists.prototype.btnActiveClickHandler = function() {

	// Обработка клика по кнопке Active
	// Показывает количество незаконченных дел

	if (this._btnActiveOn == true) {

		this.$btnDelete.innerText = "";
		this.$blockInfo.innerText = "";
		this.$btnActive.style.color = "#BEB6B6";

		for (let task in this._tasks) {
			if (this._tasks[task].taskCheckbox.checked == false) {
				this._tasks[task].taskBlock.style.boxShadow = 'none';
			}
		}

		this._btnActiveOn = false;

		return;

	}

	for (let task in this._tasks) {
		this._tasks[task].taskBlock.style.boxShadow = 'none';
	}

	for (let task in this._tasks) {
		if (this._tasks[task].taskCheckbox.checked == false) {
			this._tasks[task].taskBlock.style.boxShadow = '0 0 5px inset';
		}
	}

	this.$btnDelete.innerText = "Clear active tasks";

	this.$btnAll.style.color = "#BEB6B6";
	this.$btnActive.style.color = '#D3D3D3';
	this.$btnCompleted.style.color = '#BEB6B6';

	this._btnAllOn			= false;
	this._btnActiveOn		= true;
	this._btnCompletedOn	= false;

	// body..

	let activeCount = 0;

	for (let task in this._tasks) {
		if (this._tasks[task].taskCheckbox.checked == false) {
			activeCount += 1;
		}
	}

	if (activeCount == 0) {
		this.$blockInfo.innerText = "No tasks";
	} else if (activeCount == 1) {
		this.$blockInfo.innerText = "1 item left";
	} else {
		this.$blockInfo.innerText = activeCount + " items left";
	}

}

Todolists.prototype.btnCompletedClickHandler = function() {

	// Обработка клика по кнопке Completed
	// Показывает количество законченных дел

	if (this._btnCompletedOn == true) {

		this.$btnDelete.innerText = "";
		this.$blockInfo.innerText = "";
		this.$btnCompleted.style.color = "#BEB6B6";

		for (let task in this._tasks)
			if (this._tasks[task].taskCheckbox.checked == true)
				this._tasks[task].taskBlock.style.boxShadow = 'none';

		this._btnCompletedOn = false;

		return;

	}

	for (let task in this._tasks)
			this._tasks[task].taskBlock.style.boxShadow = 'none';

	for (let task in this._tasks)
		if (this._tasks[task].taskCheckbox.checked == true)
			this._tasks[task].taskBlock.style.boxShadow = '0 0 5px inset';

	this.$btnDelete.innerText = "Clear completed tasks";

	this.$btnAll.style.color = "#BEB6B6";
	this.$btnActive.style.color = '#BEB6B6';
	this.$btnCompleted.style.color = '#D3D3D3';

	this._btnAllOn			= false;
	this._btnActiveOn		= false;
	this._btnCompletedOn	= true;

	// body..

	let completedCount = 0;

	for (let task in this._tasks)
		if (this._tasks[task].taskCheckbox.checked == true)
			completedCount += 1;

	if (completedCount == 0)
		this.$blockInfo.innerText = "No tasks";
	else if (completedCount == 1)
		this.$blockInfo.innerText = "1 item left";
	else
		this.$blockInfo.innerText = completedCount + " items left";

}

Todolists.prototype.btnDeleteClickHandler = function() {

	// Обработка клика по кнопке удаления нескольких дел
	// Удаляет несколько дел

	if (!confirm("Delete tasks?")) {
		return;
	}

	switch(true) {

		case this._btnAllOn: {

			for (let task in this._tasks) {
				this._tasks[task].taskBlock.remove();
			}

			this._tasks 		= [];
			this._counterForId 	= 0;
			this._counterTasks 	= 0;

			this._btnAllOn = false;
			setTimeout(this.btnAllClickHandler.bind(this), 10);

			break;

		}

		case this._btnActiveOn: {

			for (var task in this._tasks)
				if (this._tasks[task].taskCheckbox.checked == false) {

					this._tasks[task].taskBlock.remove();
					this._tasks[task] = null;
					
				}

			this._tasks = this._tasks.filter((item) => item != null);
			this._counterTasks = this._tasks.length + 1;

			this._btnActiveOn = false;
			setTimeout(this.btnActiveClickHandler.bind(this), 10);

			break;
		}

		case this._btnCompletedOn: {
			
			for (var task in this._tasks)
				if (this._tasks[task].taskCheckbox.checked == true) {

					this._tasks[task].taskBlock.remove();
					this._tasks[task] = null;
					
				}

			this._tasks = this._tasks.filter((item) => item != null);
			this._counterTasks = this._tasks.length + 1;

			this._btnCompletedOn = false;
			this.btnCompletedClickHandler.call(this);

			break;
		}

	}

}

Todolists._countLists = 0;

// })()

	// 		input - поле ввода дел (Обязательный)
	//		tasks - блок хранящие дела (обязательный)
	//		taskClassName - название класса, которое будет иметь каждое дело, для стилизации (не обязательный)
	//		btnDelClassName - название класса, для кнопки удаления одного дела (не обязательный)
	//		btnAll - кнопка All, для возможности удаления всех дел
	//		btnActive - кнопка Active, для возможности удаления не выполненных дел
	//		btnCompleted - кнопка completed, для возможности удаления выполненных дел
	//		blockInfo - место для отбражения количества оставшихся дел
	//		btnDelete - кнопка для удаления нескольких дел сразу

let configForTodos1 = {

	input: 				document.querySelector(".main_todolist_input"),
	tasks: 				document.querySelector(".todolist_tasks"),
	taskClassName: 		"todolist_task",
	btnDelClassName: 	"todolist_btn_delete_task",
	btnAll: 			document.querySelector(".btn_all1"),
	btnActive: 			document.querySelector(".btn_active1"),
	btnCompleted: 		document.querySelector(".btn_completed1"),
	blockInfo: 			document.querySelector(".count_info1"),
	btnDelete: 			document.querySelector(".btn_delete1"),

}

let configForTodos2 = {

	input: 				document.querySelector(".main_todolist_input2"),
	tasks: 				document.querySelector(".todolist_tasks2"),
	taskClassName: 		"todolist_task2",
	btnDelClassName: 	"todolist_btn_delete_task2",
	btnAll: 			document.querySelector(".btn_all2"),
	btnActive: 			document.querySelector(".btn_active2"),
	btnCompleted: 		document.querySelector(".btn_completed2"),
	blockInfo: 			document.querySelector(".count_info2"),
	btnDelete: 			document.querySelector(".btn_delete2"),

}

let todolist = new Todolists('to-do-list', configForTodos1);

let todolist2 = new Todolists('to-do-list2', configForTodos2);