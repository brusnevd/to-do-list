function Todos(mainToDoListBlockClass, mainBlockId) {
	this.$blockWithAll = document.querySelector('.' + mainToDoListBlockClass);
	this.$blockOfTasks = this.$blockWithAll.querySelector('#' + mainBlockId);
	this.$inputElement = this.$blockWithAll.querySelector('input');

	this.$inputElement.blockTask = this.$blockOfTasks;

	this.$inputElement.addEventListener('keyup', function(event) {
		if (event.keyCode == 13) {
			this._taskCount++;

			let newTask = document.createElement('div');
			newTask.id = 'newTask';

			let checkboxTag = document.createElement('input');
			checkboxTag.type = 'checkbox';
			checkboxTag.id = 'checkbox' + this._taskCount;

			let labelTag = document.createElement('label');

			let text = document.createTextNode(this.value);

			labelTag.for = checkboxTag.id;
			labelTag.prepend(text);
			
			newTask.prepend(labelTag);
			newTask.prepend(checkboxTag);

			this.blockTask.prepend(newTask);
		}
	});

	// console.log(this.$blockWithAll);
	// console.log(this.$blockOfTasks);
	// console.log(this.$inputElement);
}

let todos = new Todos('mainBlock1', 'blockOfTasks1');
let todos2 = new Todos('mainBlock2', 'blockOfTasks2');