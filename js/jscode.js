window.onload = function() {

var maxCount = 0;
var count = 0;
var activeCount = 0;
var completedCount = 0;

var all = 0;
var active = 0;
var completed = 0;

var countAnswer = document.getElementById('countAnswer');

var clearComplText = document.getElementById('clearCompl');
clearComplText.onclick = function() {setTimeout(clearCompleted, 10)};

var countAll = document.getElementById('all');
// countAll.style.color = '#D3D3D3';

countAll.onclick = function() {
	if (all == 1) {
		unPressCount();
	} else {

		all = 1;
		active = 0;
		completed = 0;

		countAll.style.color = '#D3D3D3';
		countActive.style.color = '#BEB6B6';
		countCompl.style.color = '#BEB6B6';

		clearComplText.innerHTML = 'Clear all tasks';

		for (let i = 0; i <= maxCount; i++) {
			let element = document.getElementsByClassName('task');

			if (element[i]) {
				element[i].style.transitionDuration = '.1s';

				element[i].style.boxShadow = '0 0 5px inset';
			}
		}

		leftTextChange();
	}
};

var countActive = document.getElementById('active');
countActive.onclick = function() {
	if (active == 1) {
		unPressCount();
	} else {

		all = 0;
		active = 1;
		completed = 0;

		countAll.style.color = '#BEB6B6';
		countActive.style.color = '#D3D3D3';
		countCompl.style.color = '#BEB6B6';

		clearComplText.innerHTML = 'Clear active tasks';

		for (let i = 1; i <= maxCount; i++) {
			let checkboxElem = document.getElementById('checkbox' + i);

			if (checkboxElem) {
				if (!checkboxElem.checked) {
					// let element = document.getElementsByClassName('task');

					checkboxElem.parentNode.style.transitionDuration = '.1s';
					checkboxElem.parentNode.style.boxShadow = '0 0 5px inset';
				} else {
					// let element = document.getElementsByClassName('task');
					checkboxElem.parentNode.style.boxShadow = 'none';
				}
			}
		}

		leftTextChange();
	}
};

var countCompl = document.getElementById('completed');
countCompl.onclick = function() {
	if (completed == 1) {
		unPressCount();
	} else {

		all = 0;
		active = 0;
		completed = 1;

		countAll.style.color = '#BEB6B6';
		countActive.style.color = '#BEB6B6';
		countCompl.style.color = '#D3D3D3';

		clearComplText.innerHTML = 'Clear completed tasks';

		for (let i = 1; i <= maxCount; i++) {
			let checkboxElem = document.getElementById('checkbox' + i);

			if (checkboxElem) {
				if (checkboxElem.checked) {
					// let element = document.getElementsByClassName('task');

					checkboxElem.parentNode.style.transitionDuration = '.1s';
					checkboxElem.parentNode.style.boxShadow = '0 0 5px inset';
				} else {
					// let element = document.getElementsByClassName('task');
					checkboxElem.parentNode.style.boxShadow = 'none';
				}
			}
		}

		leftTextChange();
	}
};

function unPressCount() {
	clearComplText.innerHTML = '';
	countAnswer.innerHTML = '';

	all = 0;
	active = 0;
	completed = 0;

	countAll.style.color = '#BEB6B6';
	countActive.style.color = '#BEB6B6';
	countCompl.style.color = '#BEB6B6';

	for (let i = 0; i <= maxCount; i++) {
		let element = document.getElementsByClassName('task');

		if (element[i]) {
			element[i].style.boxShadow = 'none';
		}
	}
}

function leftTextChange() {
	if (all == 1) {
		if (count > 1) {
			countAnswer.innerHTML = count + ' items left';
		} else if (count == 1) {
			countAnswer.innerHTML = '1 item left';
		} else if (count == 0) {
			countAnswer.innerHTML = 'No tasks';
		}

	} else if (active == 1) {

		// ...

		activeCount = 0;
		
		for (let i = 1; i <= maxCount; i++) {
			let checkboxElem = document.getElementById('checkbox' + i);

			if (checkboxElem) {
				if (!checkboxElem.checked) {
					activeCount += 1;
				}
			}
		}

		if (activeCount > 1) {
			countAnswer.innerHTML = activeCount + ' active items';
		} else if (activeCount == 1) {
			countAnswer.innerHTML = '1 active item';
		} else if (activeCount == 0) {
			countAnswer.innerHTML = 'No active tasks';
		}

		// ...

	} else if (completed == 1) {
		activeCount = 0;
		
		for (let i = 1; i <= maxCount; i++) {
			let checkboxElem = document.getElementById('checkbox' + i);

			if (checkboxElem) {
				if (!checkboxElem.checked) {
					activeCount += 1;
				}
			}
		}

		completedCount = count - activeCount;

		if (completedCount > 1) {
			countAnswer.innerHTML = completedCount + ' completed items';
		} else if (completedCount == 1) {
			countAnswer.innerHTML = '1 completed item';
		} else if (completedCount == 0) {
			countAnswer.innerHTML = 'No completed tasks';
		}
	}
}

function clearCompleted() {
	if ((count > 0) && confirm('Clear highlighted cases?')) {

		if (all == 1) {
			for (let i = 1; i <= maxCount; i++) {
				var taskToDelete = document.getElementById('newTask');

				if (taskToDelete) {
					taskToDelete = document.getElementById('newTask');
					taskToDelete.parentNode.removeChild(taskToDelete);
				}
			}

			count = 0;
			maxCount = 0;

		} else if (active == 1) {

			for (let i = 1; i <= maxCount; i++) {
				let checkboxElem = document.getElementById('checkbox' + i);

				if (checkboxElem) {
					if (!checkboxElem.checked) {
						checkboxElem.parentNode.parentNode.removeChild(checkboxElem.parentNode);
						count -= 1;
					}
				}
			}

		} else if (completed == 1) {

			for (let i = 1; i <= maxCount; i++) {
				let checkboxElem = document.getElementById('checkbox' + i);

				if (checkboxElem) {
					if (checkboxElem.checked) {
						checkboxElem.parentNode.parentNode.removeChild(checkboxElem.parentNode);
						count -= 1;
					}
				}
			}
		}

		var allTasks = document.getElementsByClassName('task');

		if ((count < 4) && (count > 1)) {
			allTasks[count - 1].style.borderBottom = '2px solid #F5F5F5';
		}

		leftTextChange();

	}
}

function prependList() {
	var mainInput = document.getElementById("myInput");

	if (mainInput.value) {

		count += 1;
		maxCount += 1;

		var divTag = document.createElement('div');

		//parent.insertBefore(child, parent.firstChild);

		blockOfTasks.insertBefore(divTag, blockOfTasks.firstChild);
		divTag.setAttribute('id', 'newTask');

		var divClassName = 'task';
		divTag.setAttribute('class', divClassName);

		var checkboxIdName = 'checkbox' + maxCount;
		var checkboxTag = document.createElement('input');
		checkboxTag.setAttribute('type', 'checkbox');
		checkboxTag.setAttribute('id', checkboxIdName);
		checkboxTag.setAttribute('class', 'checkbox');
		// divTag.prepend(checkboxTag);

		divTag.insertBefore(checkboxTag, divTag.firstChild);


		var labelTag = document.createElement('label');
		// divTag.append(labelTag);

		divTag.appendChild(labelTag);

		labelTag.setAttribute('for', checkboxIdName);
		labelTag.innerHTML = mainInput.value;

		labelTag.onclick = function() {setTimeout(leftTextChange, 10)};


		var spanTag = document.createElement('span');
		divTag.appendChild(spanTag);
		spanTag.setAttribute('id', 'delTask');


		var spanClassName = 'delTask' + count;
		spanTag.setAttribute('class', spanClassName);

		spanTag.innerHTML = '&#10006';

		// ...

		// spanTag.addEventListener('click', function() {
		// 	spanTag.parentNode.remove();

		// 	count -= 1;

		// 	leftTextChange();

		// 	if ((count < 4) && (count > 1)) {
		// 		allTasks[count - 1].style.borderBottom = '2px solid #F5F5F5';
		// 	}
		// })

		spanTag.onclick = function() {
			spanTag.parentNode.parentNode.removeChild(spanTag.parentNode);

			count -= 1;

			leftTextChange();

			if ((count < 4) && (count > 1)) {
				allTasks[count - 1].style.borderBottom = '2px solid #F5F5F5';
			}
		}

		// ...

		var allTasks = document.getElementsByClassName('task');
		
		if (count >= 4) {
			allTasks[count - 1].style.borderBottom = 'none';
		}
	}
}

var input = document.getElementById("myInput");

// input.addEventListener("keyup", function(event) {
//   if (event.keyCode === 13) {
//   	alert('hello');
//     prependList();
//     leftTextChange();
//     document.getElementById("myInput").value = '';
//   };
// });

input.onkeyup = function(event) {
	if (event.keyCode === 13) {
		prependList();
		leftTextChange();
		document.getElementById("myInput").value = '';
	}
};

}